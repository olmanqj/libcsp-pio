# libcsp-pio-wrapper

A wrapper of The [Cubesat Space Protocol](https://github.com/libcsp/libcsp) (CSP) default 
build system (waf) to be used by [PlatformIO](https://platformio.org) build system. 


CubeSat Space Protocol, by [GomSpace ApS](http://www.gomspace.com) and AAUSAT3 Project,
is available under an LGPL 2.1 license. Source code avialable in: 
https://github.com/libcsp/libcsp.


### Available CSP options:
- disable-output:            Disable CSP output
- enable-rdp:                Enable RDP support
- enable-rdp-fast-close:     Enable fast close of RDP connections
- enable-qos:                Enable Quality of Service support
- enable-promisc:            Enable promiscuous support
- enable-crc32:              Enable CRC32 support
- enable-hmac:               Enable HMAC-SHA1 support
- enable-xtea:               Enable XTEA support
- enable-dedup:              Enable packet deduplicator
- enable-external-debug:     Enable external debug API
- enable-debug-timestamp:    Enable timestamps on debug/log
- enable-if-zmqhub:          Enable ZMQ interface
- enable-can-socketcan:      Enable Linux socketcan driver
- with-loglevel=LEVEL:       Set log level. Must be one of: ['error', 'warn', 'info', 'debug']
- with-driver-usart=DRIVER:  Build USART driver. Must be one of: [windows, linux, None]
- with-os=OS:                Set operating system. Must be one of: ['posix', 'windows', 'freertos', 'macosx']
- with-rtable=TYPE:          Set routing table type:  Must be one of: ['static', 'cidr']


See also example file 'platformio_example.ini'.


### Installation
First install [PlatformIO](https://platformio.org). Then:

#### Method 1. PlatformIO ini file.
Declare library dependencies in "platformio.ini" configuration file using lib_deps option.
~~~
[env:myenv]
lib_deps =
    libcsp=https://gitlab.com/olmanqj/libcsp-pio.git
~~~

#### Method 2. Command Line Interface.
~~~
> pio lib install https://gitlab.com/olmanqj/libcsp-pio.git
~~~
