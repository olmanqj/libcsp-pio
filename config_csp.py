#!/usr/bin/env python
# encoding: utf-8

Import('env')
import platform
import subprocess

'''
CSP wscript wrapper for platformio.ini

CubeSat Space Protocol uses Waf as Building System. This script
parses CSP building options in platformio.ini file and calls
the wscript to configure CSP for the target enviornment. 

'''

# Relative Path to waf script location
working_dir = 'libcsp/'


# Detect Current Platform to construct waf command
waf = []
if platform.system() == "Windows":
    # For Widnows Environments 
    waf.append('python -x waf')
else:
    # For Linux/MacOs Environments 
    waf.append('./waf')

# All possible CSP options
csp_options = {
    "disable-output": False,
    "enable-rdp": False,
    "enable-rdp-fast-close": False,
    "enable-qos": False,
    "enable-promisc": False,
    "enable-crc32": False,
    "enable-hmac": False,
    "enable-xtea": False,
    "enable-dedup": False,
    "enable-external-debug": False,
    "enable-debug-timestamp": False,
    "enable-if-zmqhub": False,
    "enable-can-socketcan": False,
    "with-loglevel": False,
    "with-driver-usart": False,
    "with-os": False,
    "with-rtable":  False
}


# Parse Env CSP Options to construct CSP options
csp_cmd = ['--out=./']
#Get env name (eg: linux_x86_64)
env_name = env.get("PIOENV", None)
# Get CSP Options of env
env_config = env.GetProjectConfig()
csp_config = env_config.get(f"env:{env_name}", "csp_options")
# Remove spaces, new-lines and tokenize
csp_config = csp_config.replace(" ", "")
csp_config = csp_config.split("\n")
# Add each option to options list
for item in csp_config:
    if item:
        option = item.split("=")
        if len(option) == 2:
            csp_cmd.append( f"--{option[0]}={option[1]}")
            csp_options[option[0]] = option[1]
        else:
            csp_cmd.append( f"--{option[0]}")
            csp_options[option[0]] = True


# Setup Source Code Filters
src_filter = [  "+<*.c>",
                "+<external/**/*.c>",
                "+<transport/**/*.c>",
                "+<crypto/**/*.c>",
                "+<interfaces/**/*.c>",
                "+<arch/*.c>",
                "+<rtable/csp_rtable.c>"]

if csp_options['with-os']:
    src_filter.append(f"+<arch/{csp_options['with-os']}/**/*.c>")

if csp_options['with-rtable']:
    src_filter.append(f"+<rtable/csp_rtable_{csp_options['with-rtable']}.c>")


# Setup compiler flags
c_flags = ["-std=gnu99", "-g", "-Os", "-Wall", "-Wextra", "-Wshadow", "-Wcast-align",
            "-Wwrite-strings", "-Wno-unused-parameter", "-Werror"]
c_libs=[]
if csp_options['with-os']:
    if csp_options['with-os'] == 'posix':
        c_libs += ['-lrt', '-lpthread', '-lutil']
    elif csp_options['with-os'] == 'macosx':
        c_libs += ['-lpthread']
    elif csp_options['with-os'] == 'windows':
        c_flags += ['-D_WIN32_WINNT=0x0600']


# Add socketcan
if csp_options['enable-can-socketcan']:
    src_filter.append(f"+<drivers/can/can_socketcan.c>")

# Add USART driver
if csp_options['with-driver-usart']:
    src_filter.append(f"+<drivers/usart/usart_kiss.c>")
    src_filter.append(f"+<drivers/usart/usart_{csp_options['with-driver-usart']}.c>")

# Add the Source Code Filters
env.Append(SRC_FILTER=src_filter)
# Add the compiler flags
env.Append(LIBS=c_libs)
env.Append(CFLAGS=c_flags)



# Configure CSP
waf += ['configure']
print(f"CSP configure command: {waf} {csp_cmd}")
subprocess.check_call(  waf + csp_cmd , cwd=working_dir )
print("--- End CSP configuration ---")
